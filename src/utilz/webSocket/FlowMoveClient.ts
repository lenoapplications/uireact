import {Client} from '@stomp/stompjs';
import subscriptionDetails from './FlowMoveSubscriptions';
import {FlowMoveSubscribtions} from "flow-move-client";

/**
 * FlowMoveClientObject
 *  -object that is wrapper for entier communication with server (connection,communication...)
 *
 * */
type ResponseBody = {
    destination:string,
    response:any;
};
type FlowMoveClientObject = {
    __stompClient__ : any;
    connect : Function;
    isConnected : Function;
    subscriptions : FlowMoveSubscribtions,
    subscriptionsCallbacks : any,
}

export const flowMoveClient : FlowMoveClientObject = {
    __stompClient__ : undefined,
    connect : connectToFlowMoveServer,
    isConnected : isConnectedToFlowMoveServer,
    // @ts-ignore
    subscriptions : {},
    subscriptionsCallbacks : {}
};



/*
* Function that will connect __stompClient__ object to FlowMove server
* return void
* */
function connectToFlowMoveServer(){
    flowMoveClient.__stompClient__ = new Client();
    const configuration = {
        brokerURL:"ws://localhost:8080/recorder",
        onConnect:()=>{
            console.log("Connected to flowMove server");
            initializeFlowMoveClient();
        }
    };
    flowMoveClient.__stompClient__.configure(configuration);
    flowMoveClient.__stompClient__.activate();
}


/*
* Function for initializing subscriptions and setting up a flowMoveClient
* return void
* */
function initializeFlowMoveClient(){
    for(let i = 0; i < subscriptionDetails.length; ++i){
        const baseRequestUrl = subscriptionDetails[i].baseRequestUrl;
        const baseResponseUrl = subscriptionDetails[i].baseResponseUrl;
        const baseSubscriptionKey = subscriptionDetails[i].name;
        const subsList = subscriptionDetails[i].endPoints;
        addNewEndPointToSubscriptions(baseSubscriptionKey,baseRequestUrl,baseResponseUrl,subsList);
    }
}


/*
* Function that will add new key to flowMoveClient.subscriptions.
* Key represents specific subscription(controller end point) to a server.
* Calling flowMoveClient.subscriptions.$sub[i](JSON) will send json to server on specific controller end point
* (example /app/applicationSetup/applicationSetupController/$sub[i])
*
*   -params:
*       baseSubscriptionKey : string,
*       baseRequestUrl : string,
*       baseResponseUrl : string
*       endPoints : string[],
*
* return void
* */
function addNewEndPointToSubscriptions(baseSubscriptionKey:string,baseRequestUrl:string,baseResponseUrl:string,endPoints:string[]){
    createSubscriptionListenerFunction(baseResponseUrl);
    // @ts-ignore
    flowMoveClient.subscriptions[baseSubscriptionKey] = {};
    for(let index in endPoints){
        const endPoint = endPoints[index];
        // @ts-ignore
        flowMoveClient.subscriptions[baseSubscriptionKey][endPoint] = createSendToEndPointCallbackFunction(baseRequestUrl,baseResponseUrl,endPoint);
    }
}

/*
* Function that will create and return new function that will send json object to specific subscription end point
*
*   -params:
*       baseRequestUrl : string,
*       baseResponseUrl : string,
*       endPoint : string,
*
* return function
* * */
function createSendToEndPointCallbackFunction(baseRequestUrl:string, baseResponseUrl:string ,endPoint:string){
    const completeUrl = baseRequestUrl + endPoint;
    return (object:any,callback : Function)=>{
        flowMoveClient.subscriptionsCallbacks[baseResponseUrl][endPoint] = callback;
        flowMoveClient.__stompClient__.publish({destination : completeUrl,body:JSON.stringify(object)});
    }
}


/*
* Function for creating listener function on certain subscription and proceeding serverResponse to callback function
* currently active
*   -params:
*       baseResponseUrl : string,
* return void
* * */
function createSubscriptionListenerFunction(baseResponseUrl:string){
    flowMoveClient.subscriptionsCallbacks[baseResponseUrl] = {};
    flowMoveClient.__stompClient__.subscribe(baseResponseUrl,(serverResponse:any)=>{
        const responseBody :ResponseBody = JSON.parse(serverResponse.body);
        flowMoveClient.subscriptionsCallbacks[baseResponseUrl][responseBody.destination](responseBody);
    })
}


/*
* Function that will check if  __stompClient__  is connected to FlowMove server
* return boolean
* */
function isConnectedToFlowMoveServer ():boolean{
    return flowMoveClient.__stompClient__.connected;
}

/*
* Function that will return size of callbacks on certain subscriptions with end point
* return number
* */
function checkSizeOfCallbacksOnSubscriptionWithEndPoint(baseSubscriptionName:string,endPoint:string):number{
    if(baseSubscriptionName in flowMoveClient.subscriptionsCallbacks){
        if(endPoint in flowMoveClient.subscriptionsCallbacks[baseSubscriptionName]){
            let size = 0;
            const endPointObject = flowMoveClient.subscriptionsCallbacks[baseSubscriptionName][endPoint];
            for (let key in endPointObject) {
                if (endPointObject.hasOwnProperty(key)) size++;
            }
            return size;
        }
    }
    return 0;
}











