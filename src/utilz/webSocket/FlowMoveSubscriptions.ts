import {SubscriptionDetails} from "flow-move-client";


/*
* List containing all subscriptions (controllers in spring boot perspective)
*
*   -object subscription :{
*       name: string that is going to be used as key in flowMoveClient (example : flowMoveClient.$name)
*       baseRequestUrl : string referring to main part of url pointing to one of subscription (controllers) (/applicationSetup/applicationSetupController/$rest of url)
*       baseResponseUrl : string referring to main part of url pointing to subscription response
*       subs: list of strings that are added to main part of url to give point to exact controller that needs to be called
*   }
*
* */

const subscriptionDetails : SubscriptionDetails[]= [
    {
        name : "applicationSetupSubs",
        baseRequestUrl : "/app/applicationSetup/applicationSetupController/",
        baseResponseUrl: "/flowMove/applicationSetup/applicationSetupController/applicationSetupControllerResponse",
        endPoints : [
            "startApplication"
        ]
    }
];

export default subscriptionDetails;

