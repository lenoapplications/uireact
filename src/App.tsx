import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Client} from '@stomp/stompjs'
import {flowMoveClient} from "./utilz/webSocket/FlowMoveClient";

flowMoveClient.connect();
console.log(flowMoveClient);

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <button onClick={()=>{console.log(flowMoveClient.subscriptions.applicationSetupSubs.startApplication({},(resp:any)=>{console.log(resp)}))}}>Connect</button>
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
