declare module "flow-move-client"{
    type SubscriptionDetails ={
        name:string,
        baseRequestUrl : string,
        baseResponseUrl : string,
        endPoints: string[],
    };
    type FlowMoveSubscribtions = {
        applicationSetupSubs : {
            startApplication : Function,
        }
    }
}